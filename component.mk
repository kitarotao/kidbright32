#ARDUINO_CORE_LIBS := $(patsubst $(COMPONENT_PATH)/%,%,$(sort $(dir $(wildcard $(COMPONENT_PATH)/libraries/*/*/))))

#COMPONENT_ADD_INCLUDEDIRS := cores/esp32 variants/esp32 $(ARDUINO_CORE_LIBS)
#COMPONENT_PRIV_INCLUDEDIRS := cores/esp32/libb64
#COMPONENT_SRCDIRS := cores/esp32/libb64 cores/esp32 variants/esp32 $(ARDUINO_CORE_LIBS)
#CXXFLAGS += -fno-rtti

COMPONENT_ADD_INCLUDEDIRS := include
COMPONENT_SRCDIRS := src

#COMPONENT_ADD_LDFLAGS := $(COMPONENT_PATH)/libkidbright32.a
