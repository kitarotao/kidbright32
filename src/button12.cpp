#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "kidbright32.h"
#include "button12.h"

BUTTON12::BUTTON12() {
	// gpios init
	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE; // disable interrupt
	io_conf.mode = GPIO_MODE_INPUT; //set as input mode
	io_conf.pin_bit_mask = (1ULL << BUTTON1_GPIO) | (1ULL << BUTTON2_GPIO);
	io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE; // disable pull-down mode
	io_conf.pull_up_en = GPIO_PULLUP_ENABLE; // enable pull-up mode
	gpio_config(&io_conf);
}

void BUTTON12::init(void) {
	value = 0;
	temp_value = 0;
	// clear error flag
	error = false;
	// set initialized flag
	initialized = true;
	state = s_get;
}

int BUTTON12::prop_count(void) {
	// not supported
	return 0;
}

bool BUTTON12::prop_name(int index, char *name) {
	// not supported
	return false;
}

bool BUTTON12::prop_unit(int index, char *unit) {
	// not supported
	return false;
}

bool BUTTON12::prop_attr(int index, char *attr) {
	// not supported
	return false;
}

bool BUTTON12::prop_read(int index, char *value) {
	// not supported
	return false;
}

bool BUTTON12::prop_write(int index, char *value) {
	// not supported
	return false;
}

void BUTTON12::process(Driver *drv) {
	int tmp;

	switch (state) {
		case s_get:
			temp_value = 0;
			// check sw1
			if (!gpio_get_level(BUTTON1_GPIO)) {
				temp_value |= 0x01;
			}
			// check sw2
			if (!gpio_get_level(BUTTON2_GPIO)) {
				temp_value |= 0x02;
			}
			state = s_check;
			break;

		case s_check:
			tmp = 0;
			// check sw1
			if (!gpio_get_level(BUTTON1_GPIO)) {
				tmp |= 0x01;
			}
			// check sw2
			if (!gpio_get_level(BUTTON2_GPIO)) {
				tmp |= 0x02;
			}

			if (tmp == temp_value) {
				// on sw changed
				if (temp_value != value) {
					value = temp_value;
				}
			}

			state = s_get;
			break;
	}
}

int BUTTON12::sw1_get(void) {
	return (value & 0x01);
}

int BUTTON12::sw2_get(void) {
	return ((value >> 1) & 0x01);
}

void BUTTON12::wait_sw1_pressed(void) {
	while (sw1_get() == 0) {
		vTaskDelay(50 / portTICK_RATE_MS);
    }
}

void BUTTON12::wait_sw1_released(void) {
	while (sw1_get() == 1) {
		vTaskDelay(50 / portTICK_RATE_MS);
    }
}

void BUTTON12::wait_sw2_pressed(void) {
	while (sw2_get() == 0) {
		vTaskDelay(50 / portTICK_RATE_MS);
    }
}

void BUTTON12::wait_sw2_released(void) {
	while (sw2_get() == 1) {
		vTaskDelay(50 / portTICK_RATE_MS);
    }
}

int BUTTON12::is_sw1_pressed(void) {
	return (sw1_get() == 1);
}

int BUTTON12::is_sw1_released(void) {
	return (sw1_get() == 0);
}

int BUTTON12::is_sw2_pressed(void) {
	return (sw2_get()== 1);
}

int BUTTON12::is_sw2_released(void) {
	return (sw2_get() == 0);
}

int BUTTON12::key_pressed(void) {
	if ((is_sw1_pressed()) || (is_sw2_pressed())) {
		return 1;
	}

	return 0;
}

int BUTTON12::key_released(void) {
	if ((is_sw1_released()) && (is_sw2_released())) {
		return 1;
	}

	return 0;
}
