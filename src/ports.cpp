#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "kidbright32.h"
#include "ports.h"

PORTS::PORTS() {
	gpio_config_t io_conf;

	// outputs init
	io_conf.intr_type = GPIO_INTR_DISABLE; // disable interrupt
	io_conf.mode = GPIO_MODE_OUTPUT; // set as output mode
	io_conf.pin_bit_mask = (1ULL << USBSW_GPIO) | (1ULL << OUT1_GPIO) | (1ULL << OUT2_GPIO); // pin bit mask
	io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE; // disable pull-down mode
	io_conf.pull_up_en = GPIO_PULLUP_DISABLE; // disable pull-up mode
	usbsw_write(0);
	output1_write(0);
	output2_write(0);
	gpio_config(&io_conf);

	// inputs init
	io_conf.intr_type = GPIO_INTR_DISABLE; // disable interrupt
	io_conf.mode = GPIO_MODE_INPUT; // set as input mode
	io_conf.pin_bit_mask = (1ULL << IN1_GPIO) | (1ULL << IN2_GPIO) | (1ULL << IN3_GPIO) | (1ULL << IN4_GPIO); // pin bit mask
	io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE; // disable pull-down mode
	io_conf.pull_up_en = GPIO_PULLUP_ENABLE; // enable pull-up mode
	gpio_config(&io_conf);
}

void PORTS::init(void) {
	// clear error flag
	error = false;
	// set initialized flag
	initialized = true;
}

int PORTS::prop_count(void) {
	// not supported
	return 0;
}

bool PORTS::prop_name(int index, char *name) {
	// not supported
	return false;
}

bool PORTS::prop_unit(int index, char *unit) {
	// not supported
	return false;
}

bool PORTS::prop_attr(int index, char *attr) {
	// not supported
	return false;
}

bool PORTS::prop_read(int index, char *value) {
	// not supported
	return false;
}

bool PORTS::prop_write(int index, char *value) {
	// not supported
	return false;
}

void PORTS::process(Driver *drv) {
	//
}

void PORTS::usbsw_write(int val) {
	if (val) {
		usbsw_value = 1;
	}
	else {
		usbsw_value = 0;
	}
	gpio_set_level(USBSW_GPIO, !usbsw_value); // active low
}

void PORTS::usbsw_toggle(void) {
	usbsw_write(!usbsw_value);
}

int PORTS::usbsw_read(void) {
	return usbsw_value;
}

void PORTS::output1_write(int val) {
	if (val) {
		out1_value = 1;
	}
	else {
		out1_value = 0;
	}
	gpio_set_level(OUT1_GPIO, out1_value); // active high
}

void PORTS::output1_toggle(void) {
	output1_write(!out1_value);
}

int PORTS::output1_read(void) {
	return out1_value;
}

void PORTS::output2_write(int val) {
	if (val) {
		out2_value = 1;
	}
	else {
		out2_value = 0;
	}
	gpio_set_level(OUT2_GPIO, out2_value); // active high
}

void PORTS::output2_toggle(void) {
	output2_write(!out2_value);
}

int PORTS::output2_read(void) {
	return out2_value;
}

int PORTS::input1_read(void) {
	return gpio_get_level(IN1_GPIO);
}

int PORTS::input2_read(void) {
	return gpio_get_level(IN2_GPIO);
}

int PORTS::input3_read(void) {
	return gpio_get_level(IN3_GPIO);
}

int PORTS::input4_read(void) {
	return gpio_get_level(IN4_GPIO);
}
